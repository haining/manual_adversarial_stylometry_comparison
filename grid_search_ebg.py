import os
import pickle
import chardet
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline
from functionwords import FunctionWords
from writeprints_static import WriteprintsStatic
from sklearn.model_selection import ParameterGrid
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import Normalizer, StandardScaler


import sys
sys.path.append('.')
from utilities import get_data_from_rj, get_data_from_ebg

# read in data
train_text_ebg, train_label_ebg, test_text_ebg_imi, test_label_ebg_imi = get_data_from_ebg('imitation')
_, _, test_text_ebg_obf, test_label_ebg_obf = get_data_from_ebg('obfuscation')

test_text_ebg  = test_text_ebg_imi + test_text_ebg_obf
test_label_ebg = test_label_ebg_imi + test_label_ebg_obf

# feature engineering
y_train_ebg = train_label_ebg
y_test_ebg = test_label_ebg

# writeprints static features
vec = WriteprintsStatic()
X_train_ebg_wps = vec.transform(train_text_ebg).toarray()
X_test_ebg_wps = vec.transform(test_text_ebg).toarray()

# koppel512 features
fw = FunctionWords(function_words_list='english')
X_train_ebg_koppel = np.array([fw.transform(raw) for raw in train_text_ebg])
X_test_ebg_koppel = np.array([fw.transform(raw) for raw in test_text_ebg])

# build a pipeline
pipeline_svm = Pipeline([("normalizer", Normalizer(norm="l1")),
                     ("scaler", StandardScaler()),
                     ("Linear SVM", SVC(max_iter=-1, probability=True, break_ties=True,
                                       random_state=42))])

param_grid = {'kernel': ['linear', 'poly', 'rbf'],
              'C': np.power(10, np.arange(-5, 5, dtype=float)),
              'degree': np.linspace(1, 5, 5, dtype=int),
              'gamma': ['scale', 'auto', 1, 0.1, 0.01, 0.001, 0.0001],
              'coef0': [0, 10, 100, 1000] }

search_results = []
for grid in list(ParameterGrid(param_grid)):
    search_result = {}
    pipeline_svm[2].set_params(**grid)
    pipeline_svm.fit(X_train_ebg_wps, y_train_ebg)
#     print(f'Accuray on test set is {pipeline_svm.score(X_test_ebg_wps, y_test_ebg)}')
    search_result['param'] = grid
    search_result['accuracy'] = pipeline_svm.score(X_test_ebg_wps, y_test_ebg)
    search_results.append(search_result)

pickle.dump(search_results, open('grid_search_ebg.pkl', 'wb'))


